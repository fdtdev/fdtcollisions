﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
using Physics = RotaryHeart.Lib.PhysicsExtension.Physics;

namespace com.FDT.Collisions
{
    public class CollisionsManagerBase<TMgr, THdlr, TOwner, TSetting> : Singleton<TMgr> where TMgr:CollisionsManagerBase<TMgr, THdlr, TOwner, TSetting> where THdlr:ColliderHandlerBase<TMgr, THdlr, TOwner, TSetting> where TOwner:Object where TSetting : ColliderSettingAsset
    {
        [SerializeField] private bool _autoUpdate = true;
        [System.Serializable]
        public struct CollisionData
        {
            public Collider a;
            public TOwner aOwner;
            public Collider b;
            public TOwner bOwner;
        }
        
        public List<CollisionData> currentCollisions = new List<CollisionData>();
        
        public Action<THdlr> OnRegister;
        public Action<THdlr> OnUnregister;

        public Action<Collider, Collider> OnCollision;
        
        public List<THdlr> colliders = new List<THdlr>();
        
        protected Collider[] collisionResults = new Collider[32];
        protected Dictionary<Collider, THdlr> colliderHandlerByCollider = new Dictionary<Collider, THdlr>();
        protected Dictionary<TOwner, List<THdlr>> colliderHandlersByOwner = new Dictionary<TOwner, List<THdlr>>();
        
        

        public virtual THdlr GetHandlerByCollider(Collider c)
        {
            if (colliderHandlerByCollider.ContainsKey(c))
            {
                return colliderHandlerByCollider[c];
            }
            return null;
        }

        public virtual THdlr GetHandlerByOwnerAndSetting(TOwner owner, TSetting setting)
        {
            var chandlers = colliderHandlersByOwner[owner];
            for (int i = 0; i < chandlers.Count; i++)
            {
                if (chandlers[i].settings == setting)
                    return chandlers[i];
            }
            return null;
        }
        public virtual void Register(THdlr colliderHandler)
        {
            colliders.Add(colliderHandler);
            colliderHandlerByCollider.Add(colliderHandler.collider, colliderHandler);
            if (!colliderHandlersByOwner.ContainsKey(colliderHandler.owner))
            {
                colliderHandlersByOwner.Add(colliderHandler.owner, new List<THdlr>());
            }
            colliderHandlersByOwner[colliderHandler.owner].Add(colliderHandler);
            
            if (OnRegister!= null)
                OnRegister.Invoke(colliderHandler);
        }

        public virtual void Unregister(THdlr colliderHandler)
        {
            if (OnUnregister != null)
                OnUnregister.Invoke(colliderHandler);
            colliderHandlersByOwner[colliderHandler.owner].Remove(colliderHandler);
            if (colliderHandlersByOwner[colliderHandler.owner].Count == 0)
            {
                colliderHandlersByOwner.Remove(colliderHandler.owner);
            }
            colliders.Remove(colliderHandler);
            colliderHandlerByCollider.Remove(colliderHandler.collider);
        }
        private void FixedUpdate()
        {
            if (_autoUpdate)
            {
                UpdateCollisions();
            }
        }

        protected virtual void UpdateCollisions()
        {
            ClearCollisions(currentCollisions);
            for (int i = 0; i < colliders.Count; i++)
            {
                CheckCollisions(currentCollisions, colliders[i]);
            }
        }

        protected virtual void ClearCollisions(List<CollisionData> list)
        {
            list.Clear();
        }

        public virtual void GetCollisionsFor(THdlr p0, List<CollisionData> list)
        {
            ClearCollisions(list);
            CheckCollisions(list, p0);
        }
        protected virtual void CheckCollisions(List<CollisionData> list, THdlr p0)
        {
            var settings = p0.settings;
            var c = placeholder.PhysicsExtensions.OverlapBoxNonAlloc(p0.collider as BoxCollider, collisionResults,
                settings.layerMask, Physics.PreviewCondition.Game);

            if (c > 0)
            {
                for (int i = 0; i < c; i++)
                {
                    var otherCollider = collisionResults[i];
                    TOwner bOwner = default(TOwner); 
                    if (colliderHandlerByCollider.ContainsKey(otherCollider))
                    {
                        var chandler = colliderHandlerByCollider[otherCollider];
                        bOwner = chandler.owner;
                        if (p0.owner == bOwner)
                            continue;
                        
                    }
                    AddCollision(list, p0.collider, p0.owner, otherCollider, bOwner);
                    if (OnCollision!=null)
                        OnCollision.Invoke(p0.collider, otherCollider);
                }
            }
        }

        protected virtual void AddCollision(List<CollisionData> list, Collider aCollider, TOwner aOwner, Collider bCollider, TOwner bOwner)
        {
            CollisionData cdata = new CollisionData();
            cdata.a = aCollider;
            cdata.b = bCollider;
            cdata.aOwner = aOwner;
            cdata.bOwner = bOwner;
            list.Add(cdata);
        }
    }
}