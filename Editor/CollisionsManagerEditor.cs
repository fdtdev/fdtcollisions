﻿using UnityEditor;
using UnityEngine;

namespace com.FDT.Collisions.Editor
{
    [CustomEditor(typeof(CollisionsManager))]
    public class CollisionsManagerEditor : CollisionsManagerBaseEditor<CollisionsManager, ColliderHandler, Transform, ColliderSettingAsset>
    {
            
    }
}
