﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.Collisions
{
    [CreateAssetMenu(menuName = "FDT/Collisions/ColliderSettingAsset")]
    public class ColliderSettingAsset : IDAsset
    {
        public Color color;
        public LayerMask layerMask;
    }
}