# FDT Collisions

Gereric collisions detection system


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.collisions": "https://bitbucket.org/fdtdev/fdtcollisions.git#1.0.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtcollisions/src/1.0.0/LICENSE.md)