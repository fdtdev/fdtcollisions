﻿using System.Text;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Collisions
{
    [InitializeOnLoad]
    public class CollisionsManagerBaseEditor<TMgr, THdlr, TOwner, TSetting> : UnityEditor.Editor where TMgr : CollisionsManagerBase<TMgr, THdlr, TOwner, TSetting>
        where THdlr : ColliderHandlerBase<TMgr, THdlr, TOwner, TSetting>
        where TOwner : Object
        where TSetting : ColliderSettingAsset
    {
        static CollisionsManagerBaseEditor() {
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;
        }
        static void HierarchyWindowItemOnGUI (int instanceID, Rect selectionRect) {			
            GameObject gameObject = EditorUtility.InstanceIDToObject (instanceID) as GameObject;
            DisplayActiveAndPassiveCardCount (selectionRect, gameObject);
        }
        static void DisplayActiveAndPassiveCardCount (Rect selectionRect, GameObject gameObject)
        {
            if (gameObject == null) return;
            THdlr ch = gameObject.GetComponent<THdlr>();
            if (ch == null || ch.settings == null) return;
            selectionRect.width = 15;
            ch.settings.color.a = Mathf.Min(ch.settings.color.a, 0.35f);
            EditorGUI.DrawRect(selectionRect, ch.settings.color);
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawInspector(serializedObject);
            
            TMgr cTarget = target as TMgr;
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Registered colliders:", EditorStyles.boldLabel);
            foreach (var c in cTarget.colliders)
            {
                DrawCollider(cTarget, c);
            }
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Collisions:", EditorStyles.boldLabel);
            foreach (var c in cTarget.currentCollisions)
            {
                DrawCollisionData(cTarget, c);
            }
            serializedObject.ApplyModifiedProperties();
        }

        protected virtual void DrawCollider(TMgr cTarget, THdlr ch)
        {
            EditorGUILayout.BeginHorizontal();
            Color oldColor = GUI.backgroundColor;
            if (ch.owner == null)
            {
                GUI.backgroundColor = Color.gray;
            }
            EditorGUILayout.ObjectField(ch.owner, typeof(TOwner), true);
            GUI.backgroundColor = oldColor;
            if (ch.settings != null)
            {
                GUI.backgroundColor = ch.settings.color;
            }
            EditorGUILayout.ObjectField(ch.collider, typeof(Collider), true);
            GUI.backgroundColor = oldColor;
            EditorGUILayout.EndHorizontal();
        }

        protected virtual void DrawInspector(SerializedObject o)
        {
            DrawPropertiesExcluding(serializedObject, "colliders");
        }

        protected virtual void DrawCollisionData(TMgr cTarget, CollisionsManagerBase<TMgr, THdlr, TOwner, TSetting>.CollisionData d)
        {
            EditorGUILayout.BeginHorizontal();
            Color oldColor = GUI.backgroundColor;
            THdlr ch = cTarget.GetHandlerByCollider(d.a);
            if (ch == null || ch.owner == null)
            {
                GUI.backgroundColor = Color.gray;
            }
            EditorGUILayout.ObjectField(d.aOwner, typeof(TOwner), true);
            GUI.backgroundColor = oldColor;
            if (ch != null && ch.settings != null)
            {
                GUI.backgroundColor = ch.settings.color;
            }
            EditorGUILayout.ObjectField(d.a, typeof(Collider), true);
            GUI.backgroundColor = oldColor;
            
            GUILayout.Label("  -  ");
            
            ch = cTarget.GetHandlerByCollider(d.b);
            if (ch == null || ch.owner == null)
            {
                GUI.backgroundColor = Color.gray;
            }
            EditorGUILayout.ObjectField(d.bOwner, typeof(TOwner), true);
            GUI.backgroundColor = oldColor;
            if (ch != null && ch.settings != null)
            {
                GUI.backgroundColor = ch.settings.color;
            }
            EditorGUILayout.ObjectField(d.b, typeof(Collider), true);
            GUI.backgroundColor = oldColor;
            EditorGUILayout.EndHorizontal();
        }
    }
    
   /* public class CollisionsHierarchyHandler<T, T2, T3> : UnityEditor.Editor where T : CollisionsManagerBase<T, T2, T3>
        where T2 : ColliderHandlerBase<T, T2, T3>
        where T3 : Object
    {
        private static StringBuilder sb = new StringBuilder ();
        static CollisionsHierarchyHandler() {
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;
        }

        static void HierarchyWindowItemOnGUI (int instanceID, Rect selectionRect) {			
            GameObject gameObject = EditorUtility.InstanceIDToObject (instanceID) as GameObject;
            DisplayActiveAndPassiveCardCount (selectionRect, gameObject);
        }
        static void DisplayActiveAndPassiveCardCount (Rect selectionRect, GameObject gameObject)
        {
            if (gameObject == null) return;
            T2 ch = gameObject.GetComponent<T2>();
            if (ch == null || ch.settings == null) return;
            selectionRect.width = 15;
            ch.settings.color.a = Mathf.Min(ch.settings.color.a, 0.35f);
            EditorGUI.DrawRect(selectionRect, ch.settings.color);
        }
    }*/
}
