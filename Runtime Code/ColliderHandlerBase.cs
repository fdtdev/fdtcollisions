﻿using UnityEngine;

namespace com.FDT.Collisions
{
    public class ColliderHandlerBase<TMgr, THdlr, TOwner, TSetting> : MonoBehaviour
        where TMgr : CollisionsManagerBase<TMgr, THdlr, TOwner, TSetting>
        where THdlr : ColliderHandlerBase<TMgr, THdlr, TOwner, TSetting>
        where TOwner : Object
        where TSetting : ColliderSettingAsset
    {

        [Tooltip("colliders that share owners will not register collisions")]
        public TOwner owner;

        public new Collider collider;

        public TSetting settings;

        public int _currentCollisionID;

        public int CurrentCollisionId
        {
            get { return _currentCollisionID; }
        }

        protected void OnEnable()
        {
            _currentCollisionID = UnityEngine.Random.Range(0, int.MaxValue);
            Register();
        }

        protected virtual void Register()
        {
            CollisionsManagerBase<TMgr, THdlr, TOwner, TSetting>.Instance.Register(this as THdlr);
        }

        protected void OnDisable()
        {
            Unregister();
        }

        protected virtual void Unregister()
        {
            if (CollisionsManagerBase<TMgr, THdlr, TOwner, TSetting>.Exists)
            {
                CollisionsManagerBase<TMgr, THdlr, TOwner, TSetting>.Instance.Unregister(this as THdlr);
            }
        }
    }
}