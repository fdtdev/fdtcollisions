﻿using UnityEngine;
using RotaryHeart.Lib.PhysicsExtension;
using Physics = RotaryHeart.Lib.PhysicsExtension.Physics;

namespace placeholder
{
    /*
     * 		Contributors:	
     * 		Description:	
     */
    public class PhysicsExtensions
    {
        #region Classes

        #endregion

        #region GameEvents

        //[Header("GameEvents"), SerializeField] 

        #endregion

        #region Actions

        #endregion

        #region UnityEvents

        //[Header("UnityEvents"), SerializeField] 

        #endregion

        #region Fields

        //[Header("Fields"), SerializeField] 

        #endregion

        #region Properties

        #endregion

        #region Methods

        public static int OverlapBoxNonAlloc(BoxCollider boxCollider, Collider[] results, int avoidanceLayers, RotaryHeart.Lib.PhysicsExtension.Physics.PreviewCondition previewCondition = Physics.PreviewCondition.None)
        {
            var t = boxCollider.transform;
            Vector3 c = t.position + t.TransformVector(boxCollider.center);
            Vector3 s = boxCollider.size / 2;
            var ls = t.lossyScale;
            s.x *= ls.x;
            s.y *= ls.y;
            s.z *= ls.z;
            return RotaryHeart.Lib.PhysicsExtension.Physics.OverlapBoxNonAlloc(c, s, results, t.rotation, avoidanceLayers, previewCondition);
        }

        public static int OverlapSphereNonAlloc(SphereCollider sphereCollider, Collider[] results, int avoidanceLayers, RotaryHeart.Lib.PhysicsExtension.Physics.PreviewCondition previewCondition = Physics.PreviewCondition.None)
        { 
            var t = sphereCollider.transform;
            Vector3 c = t.position + t.TransformVector(sphereCollider.center);
            float s = sphereCollider.radius;
            var ls = Mathf.Max(t.lossyScale.x,t.lossyScale.y,t.lossyScale.z) * s;

            return RotaryHeart.Lib.PhysicsExtension.Physics.OverlapSphereNonAlloc(c, ls, results, avoidanceLayers,
                previewCondition);
        }
        #endregion
    }
}